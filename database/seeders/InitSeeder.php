<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('desa')
            ->insert([
                'nama' => 'kembahang'
            ]);

        DB::table('kecamatan')
            ->insert([
                'nama' => 'batu brak'
            ]);

        DB::table('kabupaten_kota')
            ->insert([
                'nama' => 'lampung barat'
            ]);

        DB::table('provinsi')
            ->insert([
                'nama' => 'lampung'
            ]);

    }
}
