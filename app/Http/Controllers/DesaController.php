<?php

namespace App\Http\Controllers;

use App\Models\Desa;
use Illuminate\Http\Request;

class DesaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Desa::all();
        return view('index',compact('data'));
    }

    public function back(){
        return redirect ('/');
       }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        $data = new Desa();
        $data->nama = $request->get('nama');
        $data->kecamatan_id = $request->get('kecamatan_id');
        $data->kepala_desa = $request->get('kepala_desa');
        $data->luas = $request->get('luas');
        $data->satuan_luas = $request->get('satuan_luas');
        $data->populasi = $request->get('populasi');
        $data->satuan_populasi = $request->get('satuan_populasi');
        $data->situs_web = $request->get('situs_web');
        $data->save();
        return redirect ('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Desa  $desa
     * @return \Illuminate\Http\Response
     */
    public function read($id)
    {
        $data = Desa::find($id);
        return view('read',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Desa  $desa
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Desa::find($id);
        return view('edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Desa  $desa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Desa::findOrFail($id);
        $data->nama = $request->get('nama');
        $data->kecamatan_id = $request->get('kecamatan_id');
        $data->kepala_desa = $request->get('kepala_desa');
        $data->luas = $request->get('luas');
        $data->satuan_luas = $request->get('satuan_luas');
        $data->populasi = $request->get('populasi');
        $data->satuan_populasi = $request->get('satuan_populasi');
        $data->situs_web = $request->get('situs_web');
        $data->save();
        return redirect ('/')->with('alert-success','Data berhasil Diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Desa  $desa
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $data = Desa::find($id);
        $data->delete();
        return back();
    }
}
