<?php

namespace App\Http\Controllers;

use App\Models\Kabupaten;
use Illuminate\Http\Request;

class KabupatenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Kabupaten::all();
        return view('index',compact('data'));
    }

    public function back(){
        return redirect ('/');
       }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        $data = new Kabupaten();
        $data->nama = $request->get('nama');
        $data->provinsi_id = $request->get('provinsi_id');
        $data->ibu_kota_id = $request->get('ibu_kota_id');
        $data->wali_kota = $request->get('wali_kota');
        $data->luas = $request->get('luas');
        $data->satuan_luas = $request->get('satuan_luas');
        $data->populasi = $request->get('populasi');
        $data->satuan_populasi = $request->get('satuan_populasi');
        $data->situs_web = $request->get('situs_web');
        $data->save();
        return redirect ('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kabupaten  $kabupaten
     * @return \Illuminate\Http\Response
     */
    public function read($id)
    {
        $data = Kabupaten::find($id);
        return view('read',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kabupaten  $kabupaten
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Kabupaten::find($id);
        return view('edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kabupaten  $kabupaten
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Kabupaten::findOrFail($id);
        $data->nama = $request->get('nama');
        $data->provinsi_id = $request->get('provinsi_id');
        $data->ibu_kota_id = $request->get('ibu_kota_id');
        $data->wali_kota = $request->get('wali_kota');
        $data->luas = $request->get('luas');
        $data->satuan_luas = $request->get('satuan_luas');
        $data->populasi = $request->get('populasi');
        $data->satuan_populasi = $request->get('satuan_populasi');
        $data->situs_web = $request->get('situs_web');
        $data->save();
        return redirect ('/')->with('alert-success','Data berhasil Diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kabupaten  $kabupaten
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $data = Kabupaten::find($id);
        $data->delete();
        return back();
    }
}
